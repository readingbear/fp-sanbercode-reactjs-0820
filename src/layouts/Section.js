import React, {useContext} from "react"
import {
  Switch,
  Route,
  Redirect,
  Link
} from "react-router-dom";

import Home from "../pages/Home"
import ChangePassword from "../pages/ChangePassword"
import GameList from "../pages/GameList"
import About from "../pages/About"
import Movies from "../pages/Movies"
import Login from "../pages/Login"
import Register from "../pages/Register"
import {UserContext} from "../context/UserContext"


const Section = () =>{

  const [user] = useContext(UserContext);

  const PrivateRoute = ({user, ...props }) => {
    if (user) {
      return <Route {...props} />;
    } else {
      return <Redirect to="/login" />;
    }
  };

  const LoginRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;

  const RegisterRoute = ({user, ...props }) =>
  user ? <Redirect to="/" /> : <Route {...props} />;

  return(    
    <section >
      <aside>
        <div>
          <ul>
              <li class="sidebar"><Link class="sidebarlistactive" to='/'>Movies</Link></li>
              <li class="sidebar"><Link class="sidebarlist" to='/GameList'>Games</Link></li>
          </ul>
        </div>
      </aside>
      <main>
        <Switch>
          <Route exact path="/" user={user} component={Home}/>
          <Route exact path="/GameList" user={user} component={GameList}/>
          <Route exact path="/about" user={user} component={About}/>
          <LoginRoute exact path="/login" user={user} component={Login}/>
          <PrivateRoute exact path="/changepassword" user={user} component={ChangePassword}/>
          <RegisterRoute exact path="/register" user={user} component={Register}/>
          <PrivateRoute exact path="/Movies" user={user} component={Movies}/>
        </Switch>
      </main>
    </section>
  )
}

export default Section
